import express from 'express';
import { parse, write } from '../controllers/htmlparser';

const router = express.Router();
router.get('/', parse, write);

export default router;
