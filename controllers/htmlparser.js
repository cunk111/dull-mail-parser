import path from 'path';
import fs from 'file-system';
import cheerio from 'cheerio';

function getCustom(jQ) {
  const arr = [];

  jQ('.cell, .amount').each((it, elt) => {
    if (jQ(elt).text().match(/\d+(\.|\,)?\d+/g) && jQ(elt).text().match(/\d+(\.|\,)?\d+/g).length === 1) {
      arr.push({ value: parseFloat(jQ(elt).text().match(/\d+(\.|\,)?\d+/g)
        .toString()
        .replace(',', '.')),
      });
    }
  });
  return { prices: arr };
}

function getRef(jQ) {
  return jQ('.pnr-ref > .pnr-info')
    .last()
    .text()
    .trim();
}

function getOwner(jQ) {
  return jQ('.pnr-name > .pnr-info')
    .last()
    .text()
    .trim();
}

function getTotal(jQ) {
  return parseFloat(jQ('.very-important')
    .text()
    .match(/\d+(\.|\,)?\d+/g)
    .toString()
    .replace(',', '.'));
}

function roundTrips(jQ) {
  const arr = [];
  let sDate = '';

  jQ('.product-details').each((index, elt) => {
    sDate = jQ('.pnr-summary')
      .text()
      .match(/[0-9]{2}[-|\/]{1}[0-9]{2}[-|\/]{1}[0-9]{4}/g)[index]
      .split('/');

    const trainInfos = {
      departureTime: jQ(elt).find('.origin-destination-hour.segment-departure')
        .text()
        .replace('h', ':')
        .trim(),

      departureStation: jQ(elt).find('.origin-destination-station.segment-departure')
        .text()
        .trim(),

      arrivalTime: jQ(elt).find('.origin-destination-hour.segment-arrival')
        .text()
        .replace('h', ':')
        .trim(),

      arrivalStation: jQ(elt).find('.origin-destination-station.segment-arrival')
        .text()
        .trim(),

      type: jQ(elt).find('.segment')
        .first()
        .text()
        .trim(),

      number: jQ(elt).find('.segment')
        .next()
        .text()
        .match(/\d+/)
        .toString()
        .trim(),
    };
    const tDate = new Date(sDate[2]
      .concat('-', sDate[1], '-', sDate[0]))
      .toISOString()
      .replace('T', ' ');
    if (index === (jQ('.product-details').length - 1)) {
      trainInfos.passengers = [];
      jQ('.typology').each((it, val) => {
        if (it < 4) {
          trainInfos.passengers.push({
            type: 'échangeable',
            age: jQ(val)
              .text()
              .match(/\([^\)]+\)/g)
              .toString(),
          });
        }
      });
    }

    arr.push({
      type: jQ(elt)
        .find('.travel-way')
        .text()
        .trim(),
      date: tDate,
      trains: [trainInfos] });
  });
  return arr;
}

export function parse(req, res, next) {
  const html = fs.readFileSync(path.join(__dirname, '..', 'test.html'), 'utf8',
    (err) => {
      if (err) throw err;
    }).replace(/\\r\\n|\\/gi, '');

  const jQ = cheerio.load(html);
  const json = {
    status: 'ok',
    result: {
      trips: [{
        code: getRef(jQ),
        name: getOwner(jQ),
        details: {
          price: getTotal(jQ),
          roundTrips: roundTrips(jQ),
        },
      }],
      custom: getCustom(jQ),
    },
  };
  res.locals.json = json;

  next();
}

export function write(req, res) {
  fs.writeFile(path.join(__dirname, '..', 'result.json'), JSON.stringify(res.locals.json, null, 2), (err) => {
    if (err) throw err;
    res.send('done. Go fetch in project root directory for \'result.json\'');
  });
}
