import express from 'express';
import logger from 'morgan';
import router from './routes/';

const app = express();

app.use(logger('dev'));
app.use('/', router);

app.get('/favicon.ico', (req, res) => {
  res.status(204);
});

app.use((req, res, next) => {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handler
app.use((err, req, res, next) => {
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};
  res.status(err.status || 500);

  res.json({ error: err });
});

export default app;
